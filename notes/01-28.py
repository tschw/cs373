# -----------
# Mon, 28 Jan
# -----------

"""
unit tests
coverage
"""

"""
In Java:
    class A extends B ...
In Python
    class A (B) ...
"""

"""
unittest's method names must begin with "test"

In Java
    there's a special keyword, "this'

In Python
    there's NO special keyword
    the first argument of the method is analogous to "this"
    "self" by CONVENTION

In Java
    this.method() is optional
    method()      works

In Python
    self.method() is required

"""

"""
password: 2831

1. run the code and confirm success
2. identify and fix the tests
3. run the code and confirm failure
4. fix the code
5. run the code and confirm success
"""
