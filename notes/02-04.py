# -----------
# Mon,  4 Feb
# -----------

"""
ask questions on Piazza
go to the lab sessions, Tu, Thu
go to office hours
got to FoCS events
"""

"""
(3n + 1) / 2
3n/2 + 1/2
n + n/2 + 1/2
because of int div
n + n/2 + 1
"""

"""
mcl(10, 100)
b = 10
e = 100
m = 51
if 10 < 51
mcl(10, 100) == mcl(51, 100)
10, 20, 40, 80
15, 30, 60
50, 100
"""

"""
write code that's easy to test

kernel (abstract away the I/O)
    Collatz.py

run harness (I/O: stdin and stdout)
    RunCollatz.py

test harness (I/O: hardwired with unit tests)
    TestCollatz.py
"""

"""
read, eval, print loop (REPL)
"""

"""
you to need a new file, which the graders don't care about, to send to HackerRank

merge:
    Collatz.py
    RunCollatz.py

don't forget to remove import
"""

"""
LAZY cache
cache in response to reading
an array of cycle lengths
you could start with an array of 1 million
trial and error to determine the best size

EAGER cache
cache before reading
you could start with an array of 1 million
trial and error to determine the best size

META cache
cache outside of HackerRank program
it doesn't quite work with cycle lengths
too few could be stored to make a difference

cache max cycle lengths instead
what if we did
mcl(   1, 1000)
mcl(1001, 2000)
mcl(2001, 3000)
mcl(3001, 4000)
...

then
mcl(1500, 3500): without cache: 2000 calculations

with the cache: 1002 calculations
mcl(1500, 2000): 500 calculations
mcl(2001, 3000):   1 look up
mcl(3001, 3500): 500 calculations
"""
