# -----------
# Fri, 15 Feb
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Prateek
        T,   5-6pm,        GDC 1.302
    Hannah
        T,   1:30-2:30pm,  GDC 1.302
    Tesia
        W,   3-4pm,        GDC 3.302
    Uriel
        M,   3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
a container is an iterable, non-exhaustible
an iterator is an iterable, exhaustible
"""

def reduce (bf, a, v) :
    for w in a :
        v = bf(v, w)
    return v

x = range(2, 5)
print(type(x))  # range

p = iter(x)
print(type(p))  # range_iterator

v = next(p)
print(v)        # 2

class range_iterator :
    def __init__ (self, b, e) :
        self.b = b
        self.e = e

    def __iter__ (self) :
        return self

    def __next__ (self) :
        if self.b == self.e :
            raise StopIteration # or raise StopIteration()
        b = self.b
        self.b += 1
        return b
