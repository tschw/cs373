# -----------
# Fri, 25 Jan
# -----------

"""
assertions
"""

"""
Collatz Conjecture, 100 years old

take a pos int
if even, divide   by 2
if odd,  multiply by 3 and add 1
repeat until 1

5 16 8 4 2 1

cycle length of  5 is 6
cycle length of 10 is 7
"""

"""
/  is true  division, output type is always float
// is floor division, output type is the input type
"""

"""
assertions are good for preconditions and for postconditions
assertions are good for programmer errors
assertions are computational comments

assertions are not good for testing
assertions are not good for user error

unit tests are good for testing
"""
