# -----------
# Mon, 11 Mar
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Prateek
        T,   5-6pm,        GDC 1.302
    Hannah
        T,   1:30-2:30pm,  GDC 1.302
    Tesia
        W,   3-4pm,        GDC 3.302
    Uriel
        M,   3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

@debug_function
def f3 (n) :
    return n + 1

"""
1. def f3
2. debug_function(f3)
3. f3 = debug_function(f3)
"""

@callable1(...)
callable2

def make_iterable (p) :
    class c :
        def __init__ (self, *t, **d) :
            self.t = t
            self.d = d

        def __iter__ (self) :
            return p(*self.t, **self.d)
    return c
