# -----------
# Mon, 25 Feb
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Prateek
        T,   5-6pm,        GDC 1.302
    Hannah
        T,   1:30-2:30pm,  GDC 1.302
    Tesia
        W,   3-4pm,        GDC 3.302
    Uriel
        M,   3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

def f () :
    print("abc")
    yield 2
    print("def")
    yield 3
    print("ghi")

p = f() # f() does not run, magic runs, builds and returns a generator
        # a generator is an iterator
        # a generator is a  continuation

q = iter(p)
print(q is p) # true

v = next(p)   # does run f(), abc
print(v)      # 2

v = next(p)   # def
print(v)      # 3

v = next(p)   # ghi, raise StopIteration

class range_iterator1 :
    ...

x = range_iterator_1(...)
print(type(x))            # range_iterator_1
iter
next

def range_iterator_2 (start, stop) :
    while (start != stop) :
        yield start
        start += 1

p = range_iterator_2(...)
print(type(p))            # generator
iter
next

class my_range :
    def __init__ (self, start, stop) :
        self.start = start
        self.stop  = stop

    def __iter__ (self) # better not return self
        start = self.start
        while (start != self.stop) :
            yield start
            start += 1

    def __getitem__ (self, i) :
        if i < 0 :
            return self.stop + i
        return self.start + i
