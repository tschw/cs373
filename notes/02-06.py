# -----------
# Wed,  6 Feb
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm,   GDC 6.302
        Th, 3:30-4:10pm,  Zoom
    Prateek
        T,  5-6pm,        GDC 1.302
    Hannah
        T,  1:30-2:30pm,  GDC 1.302
    Tesia
        W,  3-4pm,        GDC 3.302
    Uriel
        M,  3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
list is implemented with a front-loaded array
list is heterogeneous
list is mutable
order matters
empty list is not a singleton
cost of insertion at the beg: linear
cost of insertion at the mid: linear
cost of insertion at the end: amortized const
cost of removal   at the beg: linear
cost of removal   at the mid: linear
cost of removal   at the end: const
"""

[2, 3] == [3, 2] # no
[]     is []     # no
a = [2, 2]
print(len(a))    # 2

"""
tuple is implemented with a front-loaded array
tuple is heterogeneous
tuple is immutable
order matters
empty tuple is a singleton
"""

(2, 3) == (3, 2) # no
()     is ()     # yes
a = (2, 2)
print(len(a))    # 2

"""
set is implemented with a hash table
set is heterogeneous, but elements must be hashable
the containers that are immutable are hashable
    tuple
    str
    frozenset
the containers thar at mutable are not hashable
    list
    set
    dict
set is mutable
order does NOT matter
NO duplicates
{} is NOT a set
set() is not a singleton
cost of insertion: amortized const
cost of removal:   const
"""

{2, 3} == {3, 2} # yes
set()  is set()  # no
a = {2, 2}
print(len(a))    # 1

"""
frozenset is implemented with a hash table
frozenset is heterogeneous, but elements must be hashable
frozenset is immutable
order does NOT matter
NO duplicates
frozenset() is a singleton
"""

frozenset() is frozenset() # yes

"""
dict is implemented with a hash table
dict is heterogeneous collection of key/value pairs, but keys must be hashable
dict is mutable
order does NOT matter
NO duplicate keys
{} is a dict
{} is not a singleton
cost of insertion: amortized const
cost of removal:   const
"""

{} is {} # no

"""
deque is heterogeneous
deque is a doubly linked list
deque is mutable
order matters
deque() is not a singleton
cost of insertion at the beg: const
cost of insertion at the end: const
cost of removal   at the beg: const
cost of removal   at the end: const
"""

"""
Java can have many constructors, different signatures
Python only has one constructor

Java has a closed object model
    all instances of a class have the same footprint
    all instances have the same footprint over time

Python has an open object model
    neither of the above is true
"""
