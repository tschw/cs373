# -----------
# Wed, 20 Feb
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Prateek
        T,   5-6pm,        GDC 1.302
    Hannah
        T,   1:30-2:30pm,  GDC 1.302
    Tesia
        W,   3-4pm,        GDC 3.302
    Uriel
        M,   3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
https://cloud.google.com/free/docs/map-aws-google-cloud-platform
"""

"""
IaaS
    Amazon EC2
    Google Compute Engine

PaaS
    Amazon Elastic Beanstalk
    Google App Engine
    Amazon Lambda
    Google Cloud Functions

Saas
    Gmail
    Google Maps
"""

"""
VMs        virtualizes hardware
Containers virtualize OSes
"""

"""
DBs
    Amazon Redshift
    Google BigQuery
        relational DBs are not good for sparse data and they don't scale past a couple of TBs
        BigQuery is implemented with NoSQL
        still write the queries in SQL
"""

"""
Machine Learning
    Amazon Deep Learning
    Google Tensor Flow
"""
