# -----------
# Mon, 11 Feb
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Prateek
        T,   5-6pm,        GDC 1.302
    Hannah
        T,   1:30-2:30pm,  GDC 1.302
    Tesia
        W,   3-4pm,        GDC 3.302
    Uriel
        M,   3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
some systems have tail recursion optimization
Python does NOT
systems that have it: Lisp, Scheme, Haskell
"""

for v in a :
    ...

"""
what is the nature of a?
a is iterable
for example: list, dict, set, tuple
"""

class Rizwan :
    ???

x = Rizwan()

for v in x :
    ...

# Java

TreeSet x = new TreeSet();

Iterator p = x.iterator();
Iterator q = x.iterator();

Iterator:
    hasNext
    next
    remove

ListIterator extends Iterator:
    add
    hasPrevious
    nextIndex
    previous
    previousIndex
    set

while (p.hasNext()) {
    v = p.next()
    ...}

# Python

a = [2, 3, 4]
print(type(a)) # list

p = iter(a)
print(type(p)) # list iterator

try :
    while True :
        v = next(p)
        ...
except StopIteration :
    pass

class Rizwan :
    def __iter__ (self) :
        return RizwanIterator()

class RizwanIterator :
    def __next__ (self) :
        # return the next item
        # eventually raise StopIteration

x = Rizwan()

for v in x :
    ...
