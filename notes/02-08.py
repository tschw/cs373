# -----------
# Fri,  8 Feb
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm,   GDC 6.302
        Th, 3:30-4:10pm,  Zoom
    Prateek
        T,  5-6pm,        GDC 1.302
    Hannah
        T,  1:30-2:30pm,  GDC 1.302
    Tesia
        W,  3-4pm,        GDC 3.302
    Uriel
        M,  3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
python has functions that can take other functions as arguments
and that can return functions

called higher-order functions
"""

k = (i += j) # yes: C, C++, Java; no: Python

a = [1]     * 100
a = ["abc"] * 100 # 100 references to the same string

class Daniel :
    ...

x = Daniel()

a = [x] * 100 # 100 references to the same object

"""
list's += takes on the right any iterable
list's +  only takes another list
"""

"""
is
x += y
the same as
x = x + y

NO  for lists
YES for tuples
"""

"""
tuple's  += only takes another tuple
tuples's +  only takes another tuple
"""
