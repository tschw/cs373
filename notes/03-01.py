# -----------
# Fri,  1 Mar
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Prateek
        T,   5-6pm,        GDC 1.302
    Hannah
        T,   1:30-2:30pm,  GDC 1.302
    Tesia
        W,   3-4pm,        GDC 3.302
    Uriel
        M,   3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

for v in a : # a must be iterable
    ...

a = [[2, 3], [4, 5], [6, 7]]

for u, v in a : # a must be iterable over iterables of length 2
    ...

"""
reduce is an example of a higher-order function
reduce args
    binary function
    iterable
    value

map is another example of a higher-order function
map args
    unary function
    iterable
"""
