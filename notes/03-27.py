# -----------
# Wed, 27 Mar
# -----------

"""
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Prateek
        T,   5-6pm,        GDC 1.302
    Hannah
        T,   1:30-2:30pm,  GDC 1.302
    Tesia
        W,   3-4pm,        GDC 3.302
    Uriel
        M,   3:30-4:30pm,  GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
cross join
    a relation, R
    a relation, S
returns a relation with every row in R matched to every row in S
the number of rows in the result will be the numbers of rows in R times the number of rows in S
"""

"""
theta join
    a relation, R
    a relation, S
    a binary predicate (a binary function that returns a bool)
returns a relation with every row in R matched to every row in S that satisfies the predicate
the number of rows in the result will be less than or equal to the numbers of rows in R times the number of rows in S
"""

"""
natural join
    a relation, R
    a relation, S
returns a relation with every row in R matched to every row in S that satisfies the predicate matching attributes with matching values
the number of rows in the result will be less than or equal to the numbers of rows in R times the number of rows in S
"""
