#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = too-few-public-methods

# ---------
# Slices.py
# ---------

# https://docs.python.org/3/library/functions.html#slice

class A :
    def __getitem__ (self, s) :
        if isinstance(s, slice) :
            return s.start, s.stop, s.step
        return s

def test1 () :
    x = A()
    assert x[1]             == 1
    assert list(x[2, 5])    == [2, 5]
    assert list(x[2, 5, 2]) == [2, 5, 2]

def main () :
    print("Slices.py")
    for i in range(1) :
        eval("test" + str(i + 1) + "()")
    print("Done.")

if __name__ == "__main__" : # pragma: no cover
    main()
