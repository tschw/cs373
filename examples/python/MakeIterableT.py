#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = too-few-public-methods

# ---------------
# MakeIterable.py
# ---------------

from unittest import main, TestCase

def make_iterable (iterator) :
    class iterable :
        def __init__ (self, *t, **d) :
            self.t = t
            self.d = d

        def __iter__ (self) :
            return iterator(*self.t, **self.d)

    return iterable

def range_iterator (b, e) :
    while b != e :
        yield b
        b += 1

@make_iterable
def range_iterable (b, e) :
    while b != e :
        yield b
        b += 1

map_iterable = make_iterable(map)

class MyUnitTests (TestCase) :
    def test1 (self) :
        x = range_iterator(2, 5)
        self.assertEqual(list(x), [2, 3, 4])
        self.assertEqual(list(x), [])

    def test2 (self) :
        x = range_iterable(2, 5)
        self.assertEqual(list(x), [2, 3, 4])
        self.assertEqual(list(x), [2, 3, 4])

    def test3 (self) :
        x = map(lambda v : v ** 2, (2, 3, 4))
        self.assertEqual(list(x), [4, 9, 16])
        self.assertEqual(list(x), [])

    def test4 (self) :
        x = map_iterable(lambda v : v ** 2, (2, 3, 4))
        self.assertEqual(list(x), [4, 9, 16])
        self.assertEqual(list(x), [4, 9, 16])

if __name__ == "__main__" :
    main()
